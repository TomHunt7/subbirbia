﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Collision2DEvent : UnityEvent<Collision2D> { }

public class BirbFeet : MonoBehaviour
{
    public UnityEvent<Collision2D> OnFeetCollisionEnter = new Collision2DEvent();
    public UnityEvent<Collision2D> OnFeetCollisionExit = new Collision2DEvent();
    
    public void OnCollisionEnter2D(Collision2D collision)
    {
        OnFeetCollisionEnter.Invoke(collision);
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        OnFeetCollisionExit.Invoke(collision);
    }
}
