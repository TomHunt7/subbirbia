﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nest : MonoBehaviour
{
    public int stage = 0;
    public Texture2D nestTexture;
    public SpriteRenderer renderer;

    public Texture2D nest0;
    public Texture2D nest1;
    public Texture2D nest2;
    public Texture2D nest3;
    public Texture2D nest4;
    public Texture2D nest5;

    public static Sprite nest;
    public static float width = 200;
    public static float height = 200;

    public Hud hud;

    public Birb birb;

    // Which collectibles are required for each stage?
    static Dictionary<int, ArrayList> requiredCollectibles = new Dictionary<int, ArrayList>()
    {
        {1, new ArrayList() {
            "twigs",
        }},
        {2, new ArrayList() {
            "twigs",
            "twigs",
        }},
        {3, new ArrayList() {
            "twigs",
            "mud",
            "mud",
        }},
        {4, new ArrayList() {
            "twigs",
            "mud",
            "grass",
        }},
        {5, new ArrayList() {
            "mud",
            "grass",
            "grass",
        }},
    };

    Dictionary<int, ArrayList> currentCollectiblesLeft;


    void renderNest()
    {
        Texture2D currentNest;

        switch (stage)
        {
            case 0:
                currentNest = nest0;
                break;
            case 1:
                currentNest = nest1;
                break;
            case 2:
                currentNest = nest2;
                break;
            case 3:
                currentNest = nest3;
                break;
            case 4:
                currentNest = nest4;
                break;
            case 5:
                currentNest = nest5;
                break;
            default:
                return;
        }

        // Using a spritesheet
        float x1 = (stage - 1) * width;
        float x2 = (stage) * width;

        nest = Sprite.Create(
            currentNest,
            new Rect(0, 0, 200, 200),
            new Vector2(0.5f, 0.5f),
            100
        );
        renderer.sprite = nest;
    }

    void Start()
    {
        // initialize a new set of things for the player to do
        currentCollectiblesLeft = new Dictionary<int, ArrayList>(requiredCollectibles);
        stage = 1;
        renderNest();


    }

    void Update()
    {
        updateUI();

        // Temporary for testing
        if (Input.GetKeyDown(KeyCode.N))
        {
            LevelUp();
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Collectible collectible = (Collectible)collision.gameObject.GetComponent("Collectible");


        if (collectible && !collectible.inNest)
        {

            int idx = currentCollectiblesLeft[stage].IndexOf(collectible.type);
            if (!birb.canAddToNest())
            {
                hud.setCommentary("nope");
                Destroy(collision.gameObject);

                // maybe add some splat particles here?
            }
            else if (idx > -1)
            {
                hud.setCommentary("That's just what the nest needed.");
                collectible.inNest = true;

                currentCollectiblesLeft[stage].RemoveAt(idx);
                if (currentCollectiblesLeft[stage].Count == 0)
                {
                    LevelUp();
                }
            }
            else
            {
                // Tell the player that the nest didn't need that item!
                hud.setCommentary("The nest doesn't need " + collectible.type + " right now.");
                Destroy(collision.gameObject);
            }
        }
    }

    void updateUI()
    {
        hud.setNestMeter(stage);
    }

    void LevelUp()
    {
        hud.setCommentary("Look how big the nest is!");

        if (stage < 5)
        {
            stage++;
        }
        else
        {
            Win();
        }
        renderNest();
    }

    void Win()
    {
        // Yay!
        hud.win();
    }
}
