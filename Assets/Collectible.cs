﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public string type;
    public bool inNest = false;

    static Vector3 inFrontOfThings = new Vector3(0, 0, -1);
    new Rigidbody2D rigidbody;
    public float snap = .9f;
    public float angularDrag = .5f;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        Vector3 pos = transform.position;
        pos.z = inFrontOfThings.z;
        transform.position = pos;
    }
    public void MoveTowards(Vector2 home)
    {
        Vector2 delta = home - rigidbody.position;
        rigidbody.MovePosition(rigidbody.position + delta * snap);
        rigidbody.angularVelocity *= angularDrag;
    }

}