﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class changescene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if(Input.anyKeyDown)
        {
            startGame();
        }
    }

    public void startGame()
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    public void startMenu()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }


}
