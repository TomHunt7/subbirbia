﻿using UnityEngine;
using System;
using System.Collections;

public class Bob : MonoBehaviour
{
    float originalY;

    public float magnitude = 1;

    void Start()
    {
        this.originalY = this.transform.position.y;
    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x,
            originalY + ((float)Math.Sin(Time.time) * magnitude),
            transform.position.z);
    }
}