﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleStuff : MonoBehaviour
{
    // Currently generating 5 twigs for testing the nest

    float groundHeight = -1.8f;
    //float startHeight = 2.5f;
    float spacing = 0.6f;
    float rangeLeft = -6f;
    float rangeRight = 10f;

    public Transform TwigPrefab;
    public Transform GrassPrefab;
    public Transform MudPrefab;
    public Transform WormPrefab;

    int numTwigs = 20;
    int numGrass = 20;
    int numMud = 20;

    int numWorms = 5;

    // Start is called before the first frame update
    void Start()
    {
        Generate(TwigPrefab, numTwigs, rangeLeft, rangeRight, groundHeight);
        Generate(GrassPrefab, numGrass, rangeLeft, rangeRight, groundHeight);
        Generate(MudPrefab, numMud, rangeLeft, rangeRight, groundHeight);

        //Generate(WormPrefab, 3, 10f, 12f, groundHeight - 1f);
        Generate(WormPrefab, numWorms, rangeLeft, rangeRight, groundHeight - 2f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Generate(Transform prefab, int num, float left, float right, float height)
    {

        Transform transform;
        for (float i = 0; i < num; i += 1)
        {
            float x = rangeLeft + (float)Random.Range(left, right);
            //float x = -0.3f;

            transform = Instantiate(prefab);
            //transform.localScale = new Vector2(
            //    scale + (float)(Random.value * scale - (scale / 2)),
            //    (float)(Random.value * scale * 2)
            //);

            //transform.localPosition = Vector2.right * x + Vector2.up * (startHeight + spacing * i);
            transform.localPosition = Vector2.right * x + Vector2.up * (height);
        }

    }
}