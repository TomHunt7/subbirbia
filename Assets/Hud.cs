﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hud : MonoBehaviour
{

    // ui elements
    public Text nestMeter;
    public Text energyMeter;
    public Text commentary;
    public Text winMessage;

    public BirbCalls birbCalls;

    int commentaryLifetime = 300;
    int commentaryAge = 0;

    void Start()
    {
        setCommentary("");
        winMessage.text = "";
    }

    void Update()
    {
        commentaryAge++;
        if (commentaryAge == commentaryLifetime)
        {
            setCommentary("");
        }
    }

    public void setCommentary(string comment)
    {
        commentary.text = comment.ToLower();
        birbCalls.PlayCall(comment, Vector3.zero);
        commentaryAge = 0;
    }

    public void setNestMeter(int stage)
    {
        nestMeter.text = "nest level: " + stage;
    }

    public void setEnergy(int level)
    {
        energyMeter.text = "energy: " + level;
    }

    public void win()
    {
        commentary.text = "a winning nest!";
    }
}
