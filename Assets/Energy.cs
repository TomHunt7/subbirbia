﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : MonoBehaviour
{
    public int maxLevel = 100;

    public int currentLevel;

    // Start is called before the first frame update
    void Start()
    {
        currentLevel = maxLevel;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Drain(int amount)
    {
        currentLevel = Mathf.Max(currentLevel - amount, 0);
        if (currentLevel == 0)
        {
            // lose

        }
    }

    public void Replenish(int amount)
    {
        currentLevel = Mathf.Min(currentLevel + amount, maxLevel);
    }
}
