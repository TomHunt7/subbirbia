﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirbCalls : MonoBehaviour
{
    [System.Serializable]
    public struct BirbCall
    {
        public string text;
        public AudioClip call;
    }
    public BirbCall[] calls;
    Dictionary<string, AudioClip> callLookup = new Dictionary<string, AudioClip>();

    private void Start()
    {
        foreach(BirbCall call in calls)
        {
            if (!callLookup.ContainsKey(call.text))
                callLookup.Add(call.text.ToLower(), call.call);
        }
    }
    public void PlayCall(string text, Vector3 pos)
    {
        text = text.ToLower();
        if (callLookup.ContainsKey(text))
            AudioSource.PlayClipAtPoint(callLookup[text], pos);
    }
}
