﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glowy : MonoBehaviour
{
    public float glowRate;

    enum GlowState
    {
        Off,
        On,
        Ending
    }

    GlowState glowState = GlowState.Off;


    float glowFrame = 0;

    SpriteRenderer renderer;
    Color color;

    // Start is called before the first frame update
    void Start()
    {
        renderer = (SpriteRenderer)gameObject.GetComponent("SpriteRenderer");
        color = new Color(1f, 1f, 1f, 0f);
        renderer.color = color;
    }

    // Update is called once per frame
    void Update()
    {
        if (glowState == GlowState.Off)
        {
            return;
        }

        glowFrame++;

        float currentGlowMagnitude = (Mathf.Sin(glowRate * glowFrame) + 1) / 2;
        color.a = currentGlowMagnitude;
        renderer.color = color;


        if (glowState == GlowState.Ending && currentGlowMagnitude < 0.01)
        {
            glowState = GlowState.Off;
        }

    }

    public void begin()
    {
        if (glowState == GlowState.On)
        {
            return;
        }

        if (glowState != GlowState.Ending)
        {
            glowFrame = 0;
        }

        glowState = GlowState.On;
    }

    public void end()
    {
        if (glowState == GlowState.Off)
        {
            return;
        }

        // Wait for it to fade out
        glowState = GlowState.Ending;
    }
}
