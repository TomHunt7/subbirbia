﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Trigger2DEvent : UnityEvent<Collider2D> { }

public class BirbNestDetector : MonoBehaviour
{
    public UnityEvent<Collider2D> OnNestTriggerEnter = new Trigger2DEvent();
    public UnityEvent<Collider2D> OnNestTriggerExit = new Trigger2DEvent();

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.GetComponent("Nest"))
        {
            OnNestTriggerEnter.Invoke(collider);
        }
    }

    public void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.GetComponent("Nest"))
        {
            OnNestTriggerExit.Invoke(collider);
        }
    }
}